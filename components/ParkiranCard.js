import {faMapMarkerAlt} from '@fortawesome/free-solid-svg-icons';
import {FontAwesomeIcon} from '@fortawesome/react-native-fontawesome';
import React from 'react';
import {View, Text, StyleSheet, TouchableWithoutFeedback} from 'react-native';
import {currencyFormat, threeDots} from '../utils';

export default function ParkiranCard({navigation, parkiran}) {
  return (
    <TouchableWithoutFeedback
      onPress={() =>
        navigation.navigate('Parkiran', {
          name: parkiran.name,
          id: parkiran.id,
        })
      }>
      <View style={styles.cardWrapper}>
        <View style={styles.cardHeader}>
          <Text style={styles.headerText}>{parkiran.name}</Text>
          <Text style={styles.headerText}>Biaya</Text>
        </View>
        <View style={styles.informations}>
          <View style={styles.address}>
            <View style={styles.iconWrapper}>
              <FontAwesomeIcon
                icon={faMapMarkerAlt}
                style={styles.mapIcon}
                size={12}
              />
            </View>
            <Text
              style={{flex: 1, flexWrap: 'wrap', color: 'black', fontSize: 14}}>
              {threeDots(parkiran.address, 60)}
            </Text>
          </View>
          <View style={styles.price}>
            <Text
              style={{
                fontSize: 24,
                fontWeight: 'bold',
                textAlign: 'right',
                color: 'black',
              }}>
              Rp{currencyFormat(parkiran.price)}
            </Text>
            <Text style={{textAlign: 'right', color: 'black', fontSize: 12}}>
              untuk 1 jam pertama
            </Text>
          </View>
        </View>
      </View>
    </TouchableWithoutFeedback>
  );
}

const styles = StyleSheet.create({
  cardWrapper: {
    backgroundColor: 'white',
    paddingVertical: 5,
    paddingHorizontal: 10,
    borderRadius: 10,
    width: '100%',
    height: 100,
    marginBottom: 10,
  },
  cardHeader: {
    display: 'flex',
    flexDirection: 'row',
    justifyContent: 'space-between',
  },
  headerText: {
    color: '#2196F3',
    fontWeight: 'bold',
    marginBottom: 10,
  },
  informations: {
    width: '100%',
    display: 'flex',
    flexDirection: 'row',
    justifyContent: 'space-between',
    alignContent: 'center',
  },
  address: {
    display: 'flex',
    flexDirection: 'row',
    overflow: 'hidden',
    flexGrow: 1,
  },
  iconWrapper: {
    backgroundColor: '#2196F3',
    height: 25,
    width: 25,
    display: 'flex',
    justifyContent: 'center',
    alignItems: 'center',
    borderRadius: 25 / 2,
    marginRight: 5,
  },
  mapIcon: {
    color: 'white',
  },
  price: {},
});
