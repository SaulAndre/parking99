import React, {useEffect, useState} from 'react';
import {View, Text, Image, StyleSheet, ScrollView} from 'react-native';

export default function PhotoSlider({photos}) {
  // useEffect(() => {
  //   console.log(photos);
  // });
  return (
    <ScrollView horizontal={true} style={styles.photoSliderWrapper}>
      {photos ? (
        photos.map(photo => {
          // console.log(photo);
          return (
            <View
              horizontal={true}
              style={styles.sliderItem}
              key={Math.random()}>
              <Image
                source={{
                  uri: photo,
                  width: 170,
                  height: 100,
                }}
                style={{borderRadius: 10}}
                alt="reward-img"
              />
            </View>
          );
        })
      ) : (
        <Text>Tidak ada foto</Text>
      )}
    </ScrollView>
  );
}

const styles = StyleSheet.create({
  photoSliderWrapper: {
    overflowHorizontal: 'scroll',
    whiteSpace: 'nowrap',
    display: 'flex',
    flexDirection: 'row',
    boxSizing: 'border-box',
  },
  sliderItem: {
    marginRight: 10,
  },
});
