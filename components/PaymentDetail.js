import moment from 'moment';
import React, {useEffect, useState} from 'react';
import {View, Text, StyleSheet} from 'react-native';
import {ActivityIndicator} from 'react-native-paper';
import {useAuth} from '../firebase/auth';
import {getParkiran, getUser} from '../firebase/service';
import {currencyFormat} from '../utils';

export default function PaymentDetail({
  parkiranId,
  checkIn,
  checkOut,
  handleDpChange,
}) {
  const [hours, setHours] = useState();
  const [parkiran, setParkiran] = useState();
  const [user, setUser] = useState();
  const [loading, setLoading] = useState(true);

  const calcHours = (date, checkout) => {
    if (checkout !== undefined) {
      setHours(Math.ceil((checkout - date) / (3600 * 1000)) - 1);
    } else {
      setHours(Math.ceil((new Date(Date.now()) - date) / (3600 * 1000)) - 1);
    }
  };

  const fetchData = async () => {
    await getParkiran(parkiranId).then(res => {
      setParkiran(res);
      calcHours(checkIn, checkOut);
      setLoading(false);
    });
  };

  useEffect(() => {
    if (parkiran) {
      handleDpChange(parkiran.price + hours * 1000);
    }
  }, [hours]);

  useEffect(() => {
    fetchData();
    console.log('checkIn--------------\n', checkIn);
    console.log('checkOut--------------\n', checkOut);
  }, [loading, checkIn, checkOut]);

  if (loading) {
    return (
      <View style={styles.container}>
        <ActivityIndicator size="large" color="#2196F3" />
      </View>
    );
  }

  return (
    <View style={[styles.paymentDetail]}>
      <View style={styles.detailData}>
        <View style={{}}>
          <Text
            style={{
              color: '#2196F3',
              fontSize: 18,
              fontWeight: 'bold',
              marginBottom: 10,
            }}>
            {parkiran.name}
          </Text>
          <Text style={{color: 'black', fontSize: 14}}>
            Perkiraan Check-in:&nbsp;
            <Text style={{fontWeight: 'bold', color: '#2196F3'}}>
              {moment(checkIn.toString()).format('lll')}
            </Text>
          </Text>
          <Text style={{color: 'black', fontSize: 14}}>
            Perkiraan Check-out:&nbsp;
            <Text style={{fontWeight: 'bold', color: '#2196F3'}}>
              {moment(checkOut.toString()).format('lll')}
            </Text>
          </Text>
        </View>
      </View>
      <View style={styles.calculationWrapper}>
        <View style={styles.calculation}>
          <View style={styles.calculationItem}>
            <Text style={{color: 'black'}}>1 jam pertama</Text>
            <Text style={{color: '#2196F3', fontWeight: 'bold'}}>
              Rp{currencyFormat(parkiran.price)}
            </Text>
          </View>
          {hours === 0 ? null : (
            <View style={styles.calculationItem}>
              <Text style={{color: 'black'}}>
                {hours + ' jam berikutnya'}
                <Text style={{fontWeight: 'bold', color: '#2196F3'}}>
                  {'(' + hours + 'xRp1.000)'}
                </Text>
              </Text>
              <Text style={{color: '#2196F3', fontWeight: 'bold'}}>
                Rp{currencyFormat(1000 * hours)}
              </Text>
            </View>
          )}
        </View>
        <View style={styles.total}>
          <View
            style={{
              marginTop: 5,
            }}>
            <Text
              style={{
                fontSize: 24,
                color: '#2196F3',
                fontWeight: 'bold',
                textAlign: 'right',
              }}>
              Rp{currencyFormat(parkiran.price + hours * 1000)}
            </Text>
          </View>
          <View style={{marginTop: 10}}>
            <Text
              style={{
                color: '#F50057',
                fontSize: 18,
                fontWeight: 'bold',
                marginBottom: 5,
              }}>
              Ketentuan:
            </Text>
            <Text style={{color: '#F50057', marginBottom: 5}}>
              1. Jika anda checkout melewati perkiraan jam checkout yang sudah
              dipilih, anda harus menyelesaikan pembayaran dengan penjaga
              parkir.
            </Text>
            <Text style={{color: '#F50057', marginBottom: 5}}>
              2. Jika anda checkout sebelum perkiraan jam checkout yang sudah
              dipilih, pembayaran yang sudah dilakukan tidak dapat dikurangi.
            </Text>
          </View>
        </View>
      </View>
    </View>
  );
}

const styles = StyleSheet.create({
  paymentDetail: {
    backgroundColor: 'white',
    padding: 20,
    borderRadius: 10,
    marginBottom: 15,
  },
  detailData: {},
  calculationWrapper: {
    display: 'flex',
  },
  calculation: {
    marginTop: 10,
    width: '100%',
    paddingVertical: 10,
    borderTopWidth: 2,
    borderBottomWidth: 2,
    borderColor: '#eaeaea',
  },
  calculationItem: {
    display: 'flex',
    flexDirection: 'row',
    justifyContent: 'space-between',
    marginBottom: 5,
  },
  total: {},
});
