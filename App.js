/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 *
 * @format
 * @flow strict-local
 */

import React, {useState} from 'react';
import type {Node} from 'react';
import {
  SafeAreaView,
  ScrollView,
  StatusBar,
  StyleSheet,
  Text,
  useColorScheme,
  View,
  Image,
  Button,
  TextInput,
} from 'react-native';
import {NavigationContainer} from '@react-navigation/native';
import {createNativeStackNavigator} from '@react-navigation/native-stack';
import {FontAwesomeIcon} from '@fortawesome/react-native-fontawesome';
import {faCoffee, faHeart} from '@fortawesome/free-solid-svg-icons';
import BottomNav from './components/BottomNav';
import PhotoSlider from './components/PhotoSlider';
import ParkiranCard from './components/ParkiranCard';
import CircleIcon from './components/CircleIcon';
import CustomAlert from './components/CustomAlert';
import QRCode from 'react-native-qrcode-svg';
import {generateId} from './utils';
import ScanScreen from './components/ScanScreen';
import {createUserWithEmailAndPassword} from 'firebase/auth';
import {auth, db} from './firebase/config';
import {
  collection,
  query,
  where,
  getDocs,
  setDoc,
  doc,
} from 'firebase/firestore/lite';
import firestore from '@react-native-firebase/firestore';
import Home from './screens/Home';
import SearchParkir from './screens/SearchParkir';
import Parkiran from './screens/Parkiran';
import Scan from './screens/Scan';
import Tiket from './screens/CheckIn';
import CheckIn from './screens/CheckIn';
import Ticket from './screens/Ticket';
import Login from './screens/Login';
import Signup from './screens/Signup';
import {AuthProvider} from './firebase/auth';
import Checkout from './screens/CheckOut';
import FormBooking from './screens/FormBooking';
import BookingTicket from './screens/BookingTicket';

const Stack = createNativeStackNavigator();

export default function App() {
  return (
    <NavigationContainer>
      <AuthProvider>
        <Stack.Navigator initialRouteName="Login">
          <Stack.Screen
            name="Login"
            component={Login}
            options={{
              title: 'Welcome to Parking99',
              headerStyle: {
                backgroundColor: '#2196F3',
              },
              headerTintColor: '#fff',
              headerTitleStyle: {
                fontWeight: 'bold',
              },
            }}
          />
          <Stack.Screen
            name="Signup"
            component={Signup}
            options={{
              title: 'Welcome to Parking99',
              headerStyle: {
                backgroundColor: '#2196F3',
              },
              headerTintColor: '#fff',
              headerTitleStyle: {
                fontWeight: 'bold',
              },
            }}
          />
          <Stack.Screen
            name="Home"
            component={Home}
            options={{
              title: 'Parking99',
              headerStyle: {
                backgroundColor: '#2196F3',
              },
              headerShown: false,
              headerTintColor: '#fff',
              headerTitleStyle: {
                fontWeight: 'bold',
              },
            }}
          />
          <Stack.Screen
            name="Search"
            component={SearchParkir}
            options={{
              title: '',
              headerStyle: {
                backgroundColor: '#2196F3',
              },
              headerTintColor: '#fff',
              headerTitleStyle: {
                fontWeight: 'bold',
              },
            }}
          />
          <Stack.Screen
            name="Parkiran"
            component={Parkiran}
            options={({route}) => ({
              title: route.params.name,
              headerStyle: {
                backgroundColor: '#2196F3',
              },
              headerTintColor: '#fff',
              headerTitleStyle: {
                fontWeight: 'bold',
              },
            })}
          />
          <Stack.Screen
            name="Scan"
            component={Scan}
            options={{
              title: '',
              headerStyle: {
                backgroundColor: '#2196F3',
              },
              headerTintColor: '#fff',
              headerTitleStyle: {
                fontWeight: 'bold',
              },
            }}
          />
          <Stack.Screen
            name="CheckIn"
            component={CheckIn}
            options={{
              title: 'Check-in Kendaraan',
              headerStyle: {
                backgroundColor: '#2196F3',
              },
              headerTintColor: '#fff',
              headerTitleStyle: {
                fontWeight: 'bold',
              },
            }}
          />
          <Stack.Screen
            name="Ticket"
            component={Ticket}
            options={{
              title: 'Tiket Aktif',
              headerStyle: {
                backgroundColor: '#2196F3',
              },
              headerTintColor: '#fff',
              headerTitleStyle: {
                fontWeight: 'bold',
              },
            }}
          />
          <Stack.Screen
            name="Checkout"
            component={Checkout}
            options={{
              title: 'Check-out Kendaraan',
              headerStyle: {
                backgroundColor: '#2196F3',
              },
              headerTintColor: '#fff',
              headerTitleStyle: {
                fontWeight: 'bold',
              },
            }}
          />
          <Stack.Screen
            name="FormBooking"
            component={FormBooking}
            options={{
              title: 'Form Booking',
              headerStyle: {
                backgroundColor: '#2196F3',
              },
              headerTintColor: '#fff',
              headerTitleStyle: {
                fontWeight: 'bold',
              },
            }}
          />
          <Stack.Screen
            name="BookingTicket"
            component={BookingTicket}
            options={{
              title: 'Tiket Booking',
              headerStyle: {
                backgroundColor: '#2196F3',
              },
              headerTintColor: '#fff',
              headerTitleStyle: {
                fontWeight: 'bold',
              },
            }}
          />
        </Stack.Navigator>
      </AuthProvider>
      {/* <View style={styles.app}>
        <ScrollView>
          <FontAwesomeIcon icon={faCoffee} />
          <View style={styles.appItem}>
            <PhotoSlider />
          </View>
          <View style={styles.appItem}>
            <ParkiranCard
              parkiran={{
                name: 'Parkiran Pondok Cina',
                price: 3500,
                address:
                  'Jl. Lkr. Kampus Blok Mawar No.9, Pondok Cina, Kecamatan Beji, Kota Depok, Jawa Barat 16424, Indonesia',
              }}
            />
            <Button title="Button" onPress={() => getParkiran()} />
            <CircleIcon
              icon={faHeart}
              iconSize={20}
              size={35}
              bgColor="#2196F3"
              iconColor="white"
            />
            <CustomAlert title="Test alert" />
            <QRCode value={generateId(20)} size={300} />
            <ScanScreen />
            <TextInput
              style={styles.input}
              onChangeText={setEmail}
              value={email}
              placeholder="Email"
            />
            <TextInput
              style={styles.input}
              onChangeText={setPassword}
              value={password}
              secureTextEntry={true}
              placeholder="Password"
            />
            <Button title="Sign In" onPress={handleSignIn} />
            <Text>{email}</Text>
            <Text>{password}</Text>
          </View>
        </ScrollView>
        <BottomNav />
      </View> */}
    </NavigationContainer>
  );
}

const styles = StyleSheet.create({
  app: {
    flex: 1,
    backgroundColor: '#F0F0F0',
  },
  appItem: {
    marginTop: 20,
    // paddingHorizontal: 20,
  },
  input: {
    height: 40,
    margin: 12,
    borderWidth: 1,
    padding: 10,
  },
});
