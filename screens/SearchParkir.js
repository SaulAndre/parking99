import {faMapMarkerAlt} from '@fortawesome/free-solid-svg-icons';
import React, {useEffect, useState} from 'react';
import {
  View,
  Text,
  StyleSheet,
  TextInput,
  FlatList,
  TouchableWithoutFeedback,
  ActivityIndicator,
} from 'react-native';
import CircleIcon from '../components/CircleIcon';
import {getAllParkiranName} from '../firebase/service';

export default function SearchParkir({navigation}) {
  const [query, setQuery] = useState(null);
  const [loading, setLoading] = useState(true);
  const [searchResult, setSearchResults] = useState([]);
  const [listParkiran, setListParkiran] = useState([]);

  const fetchData = async () => {
    try {
      var res = await getAllParkiranName();
      setListParkiran(res);
    } catch (err) {
      console.log(err);
    } finally {
      setLoading(false);
    }
  };

  useEffect(() => {
    fetchData();
    console.log(listParkiran);
  }, []);

  useEffect(() => {
    const results = listParkiran.filter(parkiran =>
      parkiran.name.toLowerCase().includes(query),
    );
    setSearchResults(results);
  }, [query]);

  const SearchItem = ({parkiran}) => {
    return (
      <TouchableWithoutFeedback
        key={parkiran.id}
        onPress={() =>
          navigation.navigate('Parkiran', {
            name: parkiran.name,
            id: parkiran.id,
          })
        }>
        <View style={styles.searchItem}>
          <CircleIcon
            icon={faMapMarkerAlt}
            iconSize={14}
            size={25}
            bgColor="#2196F3"
            iconColor="white"
          />
          <Text style={{color: 'black', marginLeft: 10}}>{parkiran.name}</Text>
        </View>
      </TouchableWithoutFeedback>
    );
  };
  if (loading) {
    return (
      <View style={styles.container}>
        <ActivityIndicator size="large" color="#2196F3" />
      </View>
    );
  }

  return (
    <View style={styles.searchWrapper}>
      <TextInput
        style={styles.searchInput}
        placeholder="Cari Tempat Parkir"
        onChangeText={text => setQuery(text)}
        value={query}
        placeholder="Cari tempat parkir"
        placeholderTextColor="#b7b7b7"
      />
      {query
        ? searchResult.map(parkiran => {
            return <SearchItem parkiran={parkiran} />;
          })
        : listParkiran.map(parkiran => {
            return <SearchItem key={parkiran.id} parkiran={parkiran} />;
          })}
    </View>
  );
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    justifyContent: 'center',
  },
  searchWrapper: {
    flex: 1,
    backgroundColor: '#f3f3f3',
    padding: 15,
  },
  searchInput: {
    backgroundColor: 'white',
    borderRadius: 10,
    paddingVertical: 10,
    paddingHorizontal: 15,
    fontSize: 18,
    color: '#b7b7b7',
    marginBottom: 15,
  },
  searchItem: {
    display: 'flex',
    flexDirection: 'row',
    paddingVertical: 5,
  },
});
