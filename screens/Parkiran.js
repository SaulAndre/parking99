import {
  faDirections,
  faMapMarkerAlt,
  faMapPin,
} from '@fortawesome/free-solid-svg-icons';
import React, {useEffect, useState} from 'react';
import {
  View,
  Text,
  StyleSheet,
  TouchableWithoutFeedback,
  ActivityIndicator,
} from 'react-native';
import CircleIcon from '../components/CircleIcon';
import PhotoSlider from '../components/PhotoSlider';
import {getParkiran} from '../firebase/service';
import {currencyFormat} from '../utils';

export default function Parkiran({navigation, route}) {
  const {name, id} = route.params;
  const [loading, setLoading] = useState(true);
  const [parkiran, setParkiran] = useState({
    name: '',
    address: 'Jl. Lkr. Kampus Blok Mawar',
    note: 'Gerbang warna',
    price: 3500,
  });

  const handleBookingCTA = () => {
    navigation.navigate('FormBooking', {parkiranId: id});
  };

  const fetchData = async () => {
    getParkiran(id).then(res => {
      if (!res.status) {
        setParkiran(res);
        setLoading(false);
      } else {
        console.log(res);
      }
    });
  };

  useEffect(() => {
    fetchData();
  }, []);

  if (loading) {
    return (
      <View style={styles.container}>
        <ActivityIndicator size="large" color="#2196F3" />
      </View>
    );
  }
  return (
    <View style={styles.parkiranWrapper}>
      <View style={styles.header}>
        <View style={styles.addressWrapper}>
          <CircleIcon
            icon={faMapMarkerAlt}
            iconSize={24}
            size={40}
            bgColor="white"
            iconColor="#2196F3"
          />
          <View style={styles.address}>
            <Text
              style={{
                fontSize: 14,
                color: 'white',
                marginBottom: 5,
                width: '70%',
              }}>
              {parkiran.address}
            </Text>
            <Text
              style={{
                fontSize: 14,
                fontWeight: 'bold',
                color: 'white',
                width: '70%',
              }}>
              Note: {parkiran.note}
            </Text>
          </View>
        </View>
        <View style={styles.sliderWrapper}>
          <Text
            style={{
              fontSize: 18,
              fontWeight: 'bold',
              color: 'white',
              marginBottom: 10,
            }}>
            Foto Tempat Parkir
          </Text>
          <PhotoSlider photos={parkiran.photos} />
        </View>
      </View>
      <View style={styles.body}>
        <View style={styles.biayaParkirBox}>
          <View style={styles.leftText}>
            <Text style={{fontSize: 18, fontWeight: 'bold', color: 'black'}}>
              Biaya Parkir
            </Text>
            <Text
              style={{
                fontSize: 24,
                fontWeight: 'bold',
                color: '#2196F3',
              }}>
              Rp{currencyFormat(parkiran.price)}
            </Text>
            <Text
              style={{fontSize: 14, color: 'black', flex: 1, flexWrap: 'wrap'}}>
              Untuk 1 Jam Pertama, Rp1.000 per 1 jam berikutnya
            </Text>
          </View>
          <TouchableWithoutFeedback
            onPress={() => {
              navigation.navigate('Scan', {parkiranId: id});
            }}>
            <View style={styles.checkInCTA}>
              <CircleIcon
                icon={faDirections}
                iconSize={30}
                size={50}
                bgColor="white"
                iconColor="#2196F3"
              />
              <Text
                style={{
                  fontSize: 14,
                  fontWeight: 'bold',
                  color: 'white',
                  textAlign: 'center',
                }}>
                Check-in Kendaraan
              </Text>
            </View>
          </TouchableWithoutFeedback>
        </View>
        <View style={styles.bantuan}>
          <Text
            style={{
              fontSize: 18,
              fontWeight: 'bold',
              color: 'black',
              marginBottom: 10,
            }}>
            Bantuan
          </Text>
          <View style={styles.bantuanItems}>
            <TouchableWithoutFeedback onPress={handleBookingCTA}>
              <View style={styles.bantuanItem}>
                <CircleIcon
                  icon={faMapPin}
                  iconSize={30}
                  size={50}
                  bgColor="white"
                  iconColor="#2196F3"
                />
                <Text
                  style={{
                    fontSize: 14,
                    fontWeight: 'bold',
                    color: 'white',
                    textAlign: 'center',
                  }}>
                  Booking Tempat
                </Text>
              </View>
            </TouchableWithoutFeedback>
          </View>
        </View>
      </View>
    </View>
  );
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    justifyContent: 'center',
  },
  parkiranWrapper: {},
  header: {
    padding: 15,
    paddingVertical: 20,
    backgroundColor: '#2196F3',
    borderBottomLeftRadius: 20,
    borderBottomRightRadius: 20,
  },
  addressWrapper: {
    display: 'flex',
    flexDirection: 'row',
    marginBottom: 15,
  },
  address: {
    flexGrow: 1,
    marginLeft: 10,
  },
  body: {
    padding: 15,
    paddingVertical: 20,
  },
  biayaParkirBox: {
    backgroundColor: 'white',
    padding: 10,
    borderRadius: 10,
    display: 'flex',
    flexDirection: 'row',
    justifyContent: 'space-between',
  },
  leftText: {width: '75%'},
  checkInCTA: {
    backgroundColor: '#2196F3',
    width: 80,
    display: 'flex',
    justifyContent: 'center',
    alignItems: 'center',
    padding: 5,
    paddingVertical: 10,
    borderRadius: 10,
  },
  bantuan: {
    marginTop: 15,
  },
  bantuanItems: {
    display: 'flex',
    flexDirection: 'row',
  },
  bantuanItem: {
    backgroundColor: '#2196F3',
    width: 80,
    display: 'flex',
    justifyContent: 'center',
    alignItems: 'center',
    padding: 5,
    paddingVertical: 10,
    borderRadius: 10,
  },
});
