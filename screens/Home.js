import React, {useEffect, useState} from 'react';
import {
  View,
  Text,
  StyleSheet,
  ScrollView,
  TouchableWithoutFeedback,
  ActivityIndicator,
  Button,
} from 'react-native';
import BottomNav from '../components/BottomNav';
import ParkiranCard from '../components/ParkiranCard';
import {useAuth} from '../firebase/auth';
import {timestamp} from '../firebase/config';
import {
  getActiveTicket,
  getAllParkiran,
  getAllParkiranName,
  getBookingTicket,
  getParkiran,
} from '../firebase/service';
import QRCode from 'react-native-qrcode-svg';
import moment from 'moment';

export default function Home({navigation}) {
  const [loading, setLoading] = useState(true);
  const [activeTicket, setActiveTicket] = useState();
  const [bookingTicket, setBookingTicket] = useState();
  const [currentParking, setCurrentParking] = useState();
  const [listParkiran, setListParkiran] = useState([]);
  const {currentUser, logout} = useAuth();
  const handleSearch = () => {
    navigation.navigate('Search');
  };
  // getActiveTicket().then(res => setActiveTicket(res));

  const fetchData = async () => {
    try {
      await getAllParkiran().then(res => {
        setListParkiran(res);
      });
      await getActiveTicket(currentUser.uid).then(res => {
        setActiveTicket(res);
        console.log('active: -------------------\n', res);
        // handleActiveTicket();
      });
      await getBookingTicket(currentUser.uid).then(res => {
        setBookingTicket(res);
        console.log('booking----------------\n', res);
      });
    } catch (err) {
      console.log(err);
    } finally {
      setLoading(false);
    }
  };

  const handleActiveTicket = () => {
    getParkiran(activeTicket.parkiranId).then(res => {
      setCurrentParking(res);
    });
  };

  useEffect(() => {
    const unsubscribe = navigation.addListener('focus', () => {
      setLoading(true);
      fetchData();
    });
    return unsubscribe;
  }, [navigation]);

  useEffect(() => {
    fetchData();
    console.log(currentUser);
  }, []);

  if (loading)
    return (
      <View style={styles.container}>
        <ActivityIndicator></ActivityIndicator>
      </View>
    );

  return (
    <View style={styles.homeWrapper}>
      <ScrollView style={styles.content}>
        {/* <Text>{activeTicket.id}</Text> */}
        <View style={styles.header}>
          <Text style={{fontSize: 24, color: 'white'}}>Cari</Text>
          <Text style={{fontSize: 24, fontWeight: 'bold', color: 'white'}}>
            Tempat Parkiran Terdekat
          </Text>
          {/* <Button
            title="logout"
            onPress={() => {
              logout();
              navigation.navigate('Login');
            }}
          /> */}
          <TouchableWithoutFeedback onPress={handleSearch}>
            <View style={styles.searchBar}>
              <Text style={{fontSize: 18, color: '#b7b7b7'}}>
                Cari tempat parkir
              </Text>
            </View>
          </TouchableWithoutFeedback>
        </View>
        <View style={styles.body}>
          {activeTicket ? (
            <View>
              <Text
                style={{
                  fontSize: 18,
                  color: 'black',
                  fontWeight: 'bold',
                  marginBottom: 10,
                }}>
                Tiket Aktif
              </Text>
              <TouchableWithoutFeedback
                onPress={() =>
                  navigation.navigate('Checkout', {
                    ticketId: activeTicket.ticketId,
                    parkiranId: activeTicket.parkiranId,
                  })
                }>
                <View style={styles.activeTicket}>
                  <QRCode value={activeTicket.ticketId} size={50} />
                  <View style={styles.activeTicketDetail}>
                    <Text style={{color: 'black', fontSize: 14}}>
                      Check-in:&nbsp;
                      <Text style={{fontWeight: 'bold', color: '#2196F3'}}>
                        {moment(activeTicket.checkIn.toDate()).format('lll')}
                      </Text>
                    </Text>
                    <Text style={{color: 'black'}}>
                      Nomor Kendaraan:&nbsp;
                      <Text style={{fontWeight: 'bold', color: '#2196F3'}}>
                        {activeTicket.plateNumber}
                      </Text>
                    </Text>
                  </View>
                </View>
              </TouchableWithoutFeedback>
            </View>
          ) : null}

          {bookingTicket ? (
            <View>
              <Text
                style={{
                  fontSize: 18,
                  color: 'black',
                  fontWeight: 'bold',
                  marginBottom: 10,
                }}>
                Tiket Booking
              </Text>
              <TouchableWithoutFeedback
                onPress={() =>
                  navigation.navigate('BookingTicket', {
                    ticketId: bookingTicket.ticketId,
                    parkiranId: bookingTicket.parkiranId,
                  })
                }>
                <View style={styles.activeTicket}>
                  <QRCode value={bookingTicket.ticketId} size={50} />
                  <View style={styles.activeTicketDetail}>
                    <Text style={{color: 'black', fontSize: 14}}>
                      Est. Check-in:&nbsp;
                      <Text style={{fontWeight: 'bold', color: '#2196F3'}}>
                        {moment(bookingTicket.estCheckIn.toDate()).format(
                          'LLL',
                        )}
                      </Text>
                    </Text>
                    <Text style={{color: 'black'}}>
                      Nomor Kendaraan:&nbsp;
                      <Text style={{fontWeight: 'bold', color: '#2196F3'}}>
                        {bookingTicket.plateNumber}
                      </Text>
                    </Text>
                  </View>
                </View>
              </TouchableWithoutFeedback>
            </View>
          ) : null}
          {loading ? (
            <ActivityIndicator size="large" color="#2196F3" />
          ) : (
            <View>
              <Text
                style={{
                  fontSize: 18,
                  color: 'black',
                  fontWeight: 'bold',
                  marginBottom: 10,
                }}>
                Parkiran tersedia
              </Text>
              {listParkiran.map(parkiran => (
                <ParkiranCard
                  key={parkiran.id}
                  parkiran={parkiran}
                  navigation={navigation}
                />
              ))}
            </View>
          )}
        </View>
        <Text></Text>
      </ScrollView>
      <BottomNav navigation={navigation} />
    </View>
  );
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    justifyContent: 'center',
  },
  homeWrapper: {
    flex: 1,
  },
  content: {
    // padding: 10,
  },
  header: {
    backgroundColor: '#2196F3',
    padding: 15,
    paddingVertical: 25,
    paddingTop: 15,
    borderBottomRightRadius: 20,
    borderBottomLeftRadius: 20,
  },
  searchBar: {
    backgroundColor: 'white',
    paddingVertical: 10,
    paddingHorizontal: 15,
    borderRadius: 10,
    marginTop: 15,
  },
  body: {
    padding: 15,
  },
  activeTicket: {
    marginBottom: 15,
    backgroundColor: 'white',
    padding: 15,
    borderRadius: 10,
    display: 'flex',
    flexDirection: 'row',
    alignItems: 'center',
  },
  activeTicketDetail: {
    marginLeft: 15,
  },
});
