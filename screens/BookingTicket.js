import React, {useEffect, useState} from 'react';
import {
  View,
  Text,
  ScrollView,
  StyleSheet,
  ActivityIndicator,
  Button,
} from 'react-native';
import QRCode from 'react-native-qrcode-svg';
import CustomAlert from '../components/CustomAlert';
import DetailTicket from '../components/DetailTicket';
import {
  getBookingTicket,
  getParkiran,
  getTicket,
  getUser,
} from '../firebase/service';
import moment from 'moment';
import {useAuth} from '../firebase/auth';
import {currencyFormat} from '../utils';

export default function BookingTicket({navigation, route}) {
  const [loading, setLoading] = useState(true);
  const [parkiran, setParkiran] = useState(null);
  const [bookingTicket, setBookingTicket] = useState();
  const [ticket, setTicket] = useState(null);
  const {ticketId} = route.params;
  const {currentUser} = useAuth();

  const fetchData = async () => {
    await getTicket(ticketId).then(res => {
      setTicket(res);
      getParkiran(res.parkiranId).then(res2 => {
        console.log('res2-----------\n', res2);
        setParkiran(res2);
      });
    });
    await getBookingTicket(currentUser.uid).then(res => {
      setBookingTicket(res);
      setLoading(false);
    });
  };

  useEffect(() => {
    setLoading(true);
    fetchData();
  }, []);

  if (loading)
    return (
      <View style={styles.container}>
        <ActivityIndicator size="large" color="#2196F3" />
      </View>
    );
  return (
    <ScrollView style={styles.ticketWrapper}>
      {/* <Text style={{color: 'black'}}>{ticketId}</Text> */}
      <CustomAlert title="Booking tempat parkir berhasil" />
      <View style={[styles.detailTicketWrapper, {borderColor: '#2196F3'}]}>
        <View style={styles.qrWrapper}>
          <QRCode
            value={
              `{"bookingId":"` +
              bookingTicket.id +
              `", "ticketId":"` +
              bookingTicket.ticketId +
              `","userId":"` +
              currentUser.uid +
              `","plateNumber":"` +
              bookingTicket.plateNumber +
              `"}`
            }
            size={300}
          />
        </View>
        <View style={styles.detailData}>
          <View style={{width: 300}}>
            <Text
              style={{
                color: 'black',
                fontSize: 18,
                fontWeight: 'bold',
                marginBottom: 10,
              }}>
              {parkiran ? parkiran.name : null}
            </Text>
            <Text style={{color: 'black'}}>
              Id Tiket:&nbsp;
              <Text style={{fontWeight: 'bold', color: '#2196F3'}}>
                {ticket.id}
              </Text>
            </Text>
            <Text style={{color: 'black', fontSize: 14}}>
              Perkiraan Check-in:&nbsp;
              <Text style={{fontWeight: 'bold', color: '#2196F3'}}>
                {moment(bookingTicket.estCheckIn.toDate()).format('lll')}
              </Text>
            </Text>
            <Text style={{color: 'black', fontSize: 14}}>
              Perkiraan Check-out:&nbsp;
              <Text style={{fontWeight: 'bold', color: '#2196F3'}}>
                {moment(bookingTicket.estCheckOut.toDate()).format('lll')}
              </Text>
            </Text>
            <Text style={{color: 'black'}}>
              Nomor Kendaraan:&nbsp;
              <Text style={{fontWeight: 'bold', color: '#2196F3'}}>
                {bookingTicket.plateNumber}
              </Text>
            </Text>
            <Text style={{color: 'black'}}>
              Bayar di muka:&nbsp;
              <Text style={{fontWeight: 'bold', color: '#2196F3'}}>
                Rp{currencyFormat(bookingTicket.downPayment)}
              </Text>
            </Text>
          </View>
        </View>
      </View>
      <View style={{marginBottom: 50}}>
        <Button
          title="Kembali Ke Beranda"
          onPress={() => navigation.navigate('Home')}
        />
      </View>
    </ScrollView>
  );
}

const styles = StyleSheet.create({
  ticketWrapper: {
    padding: 30,
    paddingBottom: 100,
  },
  container: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
    height: 400,
  },
  detailTicketWrapper: {
    marginVertical: 15,
    padding: 20,
    display: 'flex',
    borderWidth: 3,
    borderRadius: 10,
    backgroundColor: 'white',
  },
  qrWrapper: {
    marginBottom: 20,
    display: 'flex',
    justifyContent: 'center',
    alignItems: 'center',
  },
  detailData: {
    display: 'flex',
    alignItems: 'center',
  },
});
