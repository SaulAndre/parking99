import React, {useEffect, useState} from 'react';
import {View, Text, StyleSheet, Button} from 'react-native';
import CustomAlert from '../components/CustomAlert';
import DetailTicket from '../components/DetailTicket';
import {getParkiran, getTicket} from '../firebase/service';

export default function CheckIn({navigation, route}) {
  const [ticket, setTicket] = useState();
  const {ticketId} = route.params;

  const fetchData = () => {
    getTicket(ticketId).then(res => {
      setTicket(res);
    });
  };

  useEffect(() => {
    // fetchData();
  }, []);

  return (
    <View style={styles.checkInWrapper}>
      <CustomAlert title="Check-in Berhasil" />
      <DetailTicket ticketId={ticketId} borderColor="#2196F3" />
      <Button
        onPress={() => navigation.navigate('Home')}
        title="Kembali ke Beranda"
      />
    </View>
  );
}

const styles = StyleSheet.create({
  checkInWrapper: {
    padding: 30,
  },
});
