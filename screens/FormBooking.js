import React, {useEffect, useState} from 'react';
import {
  View,
  Text,
  StyleSheet,
  Button,
  TouchableWithoutFeedback,
  ScrollView,
} from 'react-native';
import DateTimePicker from '@react-native-community/datetimepicker';
import PaymentDetail from '../components/PaymentDetail';
import {useAuth} from '../firebase/auth';
import {timestamp} from '../firebase/config';
import {getUser, setBooking} from '../firebase/service';

export default function FormBooking({navigation, route}) {
  const [isCheckInPickerShow, setIsCheckInPickerShow] = useState(false);
  const [checkIn, setCheckIn] = useState('date');
  const [isCheckOutPickerShow, setIsCheckOutPickerShow] = useState(false);
  const [checkOut, setCheckOut] = useState('date');
  const [user, setUser] = useState();
  const [dp, setDp] = useState();
  const {parkiranId} = route.params;
  const {currentUser} = useAuth();

  const showCheckInPicker = () => {
    setIsCheckInPickerShow(true);
  };

  const showCheckOutPicker = () => {
    setIsCheckOutPickerShow(true);
  };

  const onCheckIn = (event, value) => {
    console.log('event----------\n', event);
    console.log(value);
    console.log('value----------\n', value);
    if (event.type === 'dismissed') {
      setIsCheckInPickerShow(false);
      return;
    }
    setCheckIn(value);
    if (Platform.OS === 'android') {
      setIsCheckInPickerShow(false);
    }
  };

  const onCheckOut = (event, value) => {
    if (event.type === 'dismissed') {
      setIsCheckOutPickerShow(false);
      return;
    }
    setCheckOut(value);
    if (Platform.OS === 'android') {
      setIsCheckOutPickerShow(false);
    }
  };

  const handleDpChange = price => {
    console.log(price);
    setDp(price);
  };
  const handleBooking = async () => {
    await setBooking(
      parkiranId,
      user.id,
      checkIn,
      checkOut,
      dp,
      user.plateNumber,
    ).then(res => {
      console.log('response-------\n', res);
      navigation.navigate('BookingTicket', {ticketId: res});
    });
    console.log({
      id: 'bookingTicketId',
      parkiranId: parkiranId,
      userId: user.id,
      createAt: timestamp.now(),
      checkIn: timestamp.fromDate(checkIn),
      checkOut: timestamp.fromDate(checkOut),
      // plateNumber: user.plateNumber,
      dp: dp,
    });
  };

  const fetchData = async () => {
    await getUser(currentUser.uid).then(res => {
      setUser(res);
    });
  };

  useEffect(() => {
    fetchData();
  }, []);
  return (
    <ScrollView>
      <View style={styles.formBookingScreenWrapper}>
        <Text
          style={{
            fontSize: 16,
            color: '#F50057',
            fontWeight: 'bold',
            marginBottom: 10,
          }}>
          Booking hanya dapat dilakukan untuk hari yang sama.
        </Text>
        <View style={styles.formWrapper}>
          <Text
            style={{
              fontSize: 18,
              color: 'black',
              fontWeight: 'bold',
              marginBottom: 10,
            }}>
            Perkiraan Check-in
          </Text>
          <TouchableWithoutFeedback onPress={showCheckInPicker}>
            <View style={styles.timePicker}>
              <Text style={{fontSize: 16, color: 'black'}}>
                {checkIn === 'date' ? (
                  <Text style={{fontSize: 16, color: '#d4d4d4'}}>
                    Pilih jam check-in
                  </Text>
                ) : (
                  checkIn.toString()
                )}
              </Text>
            </View>
          </TouchableWithoutFeedback>
          {/* The date picker */}
          {isCheckInPickerShow && (
            <DateTimePicker
              value={checkIn === 'date' ? new Date(Date.now()) : checkIn}
              mode={'time'}
              display={Platform.OS === 'ios' ? 'spinner' : 'default'}
              is24Hour={true}
              onChange={onCheckIn}
              style={styles.datePicker}
            />
          )}
          <Text
            style={{
              fontSize: 18,
              color: 'black',
              fontWeight: 'bold',
              marginBottom: 10,
            }}>
            Perkiraan Check-out
          </Text>
          <TouchableWithoutFeedback onPress={showCheckOutPicker}>
            <View style={styles.timePicker}>
              <Text style={{fontSize: 16, color: 'black'}}>
                {checkOut === 'date' ? (
                  <Text style={{fontSize: 16, color: '#d4d4d4'}}>
                    Pilih jam check-out
                  </Text>
                ) : (
                  checkOut.toString()
                )}
              </Text>
            </View>
          </TouchableWithoutFeedback>
          {/* The date picker */}
          {isCheckOutPickerShow && (
            <DateTimePicker
              value={checkOut === 'date' ? new Date(Date.now()) : checkOut}
              mode={'time'}
              display={Platform.OS === 'ios' ? 'spinner' : 'default'}
              is24Hour={true}
              onChange={onCheckOut}
              style={styles.datePicker}
            />
          )}
          <Text
            style={{
              fontSize: 18,
              color: 'black',
              fontWeight: 'bold',
              marginBottom: 10,
            }}>
            Detail Pembayaran
          </Text>
          <PaymentDetail
            parkiranId={parkiranId}
            checkIn={checkIn === 'date' ? new Date(Date.now()) : checkIn}
            checkOut={checkOut === 'date' ? new Date(Date.now() + 1) : checkOut}
            handleDpChange={handleDpChange}
          />
          <Text style={{color: 'black', marginBottom: 10}}>
            Dengan menekan tombol di bawah ini anda setuju dengan ketentuan yang
            telah ditetapkan oleh{' '}
            <Text style={{fontWeight: 'bold', color: '#2196F3'}}>
              parking99
            </Text>
          </Text>
          <Button title="Pembayaran" onPress={handleBooking} />
        </View>
      </View>
    </ScrollView>
  );
}

const styles = StyleSheet.create({
  formBookingScreenWrapper: {
    padding: 20,
  },
  timePicker: {
    backgroundColor: 'white',
    paddingVertical: 15,
    paddingHorizontal: 15,
    borderRadius: 10,
    marginBottom: 15,
  },
});
