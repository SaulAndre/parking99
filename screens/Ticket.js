import React, {useEffect, useState} from 'react';
import {View, Text, StyleSheet, Button} from 'react-native';
import {ActivityIndicator} from 'react-native-paper';
import CustomAlert from '../components/CustomAlert';
import DetailTicket from '../components/DetailTicket';
import {getTicket} from '../firebase/service';

export default function Ticket({navigation, route}) {
  const [loading, setLoading] = useState(true);
  const [ticket, setTicket] = useState(null);
  const {ticketId} = route.params;

  const fetchData = () => {
    getTicket(ticketId)
      .then(res => {
        setTicket(res);
        console.log(res);
        setLoading(false);
      })
      .catch(err => {
        console.log(err);
      });
  };

  useEffect(() => {
    fetchData();
  }, []);

  if (loading) return <ActivityIndicator size="large" color="#2196F3" />;
  else if (ticket.status === 'paid') {
    navigation.navigate('Checkout', {
      ticketId: ticketId,
      parkiranId: ticket.parkiranId,
    });
  }
  return (
    <View style={styles.ticketWrapper}>
      <CustomAlert title="Tiket Belum Dibayar" type="danger" />
      <DetailTicket ticketId={ticketId} borderColor={'#F50057'} />
      <Button
        title="Checkout"
        onPress={() =>
          navigation.navigate('Checkout', {
            ticketId: ticketId,
            parkiranId: ticket.parkiranId,
          })
        }
      />
    </View>
  );
}

const styles = StyleSheet.create({
  ticketWrapper: {
    padding: 30,
  },
});
