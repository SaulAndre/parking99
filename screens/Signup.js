import React, {useState} from 'react';
import {TouchableOpacity, StyleSheet, View, Text, Button} from 'react-native';
import TextInput from '../components/TextInput';
import {emailValidator, fieldValidator, passwordValidator} from '../utils';
import {useAuth} from '../firebase/auth';

export default function Signup({navigation}) {
  const [email, setEmail] = useState({value: '', error: ''});
  const [password, setPassword] = useState({value: '', error: ''});
  const [name, setName] = useState({value: '', error: ''});
  const [plateNumber, setPlateNumber] = useState({value: '', error: ''});
  const [error, setError] = useState(null);
  const {signup, currentUser} = useAuth();

  const onSignupPressed = () => {
    const emailError = emailValidator(email.value);
    const passwordError = passwordValidator(password.value);
    const nameError = fieldValidator(name.value);
    const plateError = fieldValidator(plateNumber.value);
    if (emailError || passwordError || nameError || plateError) {
      setEmail({...email, error: emailError});
      setPassword({...password, error: passwordError});
      setName({...name, error: nameError});
      setPlateNumber({...plateNumber, error: plateError});
      return;
    }
    try {
      signup(email.value, password.value, name.value, plateNumber.value).then(
        res => {
          navigation.navigate('Home');
        },
      );
    } catch (err) {
      console.log(err);
      if (err.code === 'auth/user-not-found') {
        setError("Couldn't find your account.");
      }
    }
  };

  if (currentUser) navigation.navigate('Home');
  return (
    <View style={styles.loginWrapper}>
      <Text
        style={{
          fontSize: 18,
          color: 'white',
          fontWeight: 'bold',
          marginBottom: 20,
        }}>
        Welcome to parking99. Sign up to start!
      </Text>
      <Text>{error}</Text>
      <TextInput
        label="Email"
        returnKeyType="next"
        value={email.value}
        onChangeText={text => setEmail({value: text, error: ''})}
        error={!!email.error}
        errorText={email.error}
        autoCapitalize="none"
        autoCompleteType="email"
        textContentType="emailAddress"
        keyboardType="email-address"
        placeholder="Email"
        placeholderTextColor="#b7b7b7"
      />
      <TextInput
        label="Password"
        returnKeyType="done"
        value={password.value}
        onChangeText={text => setPassword({value: text, error: ''})}
        error={!!password.error}
        errorText={password.error}
        placeholder="Password"
        placeholderTextColor="#b7b7b7"
        secureTextEntry
      />
      <TextInput
        label="Name"
        returnKeyType="done"
        value={name.value}
        onChangeText={text => setName({value: text, error: ''})}
        error={!!name.error}
        errorText={name.error}
        placeholder="Name"
        placeholderTextColor="#b7b7b7"
      />
      <TextInput
        label="Plate Number"
        returnKeyType="done"
        value={plateNumber.value}
        onChangeText={text => setPlateNumber({value: text, error: ''})}
        error={!!plateNumber.error}
        errorText={plateNumber.error}
        placeholder="Plate Number"
        placeholderTextColor="#b7b7b7"
      />
      {/* <View style={styles.forgotPassword}>
        <TouchableOpacity
          onPress={() => navigation.navigate('ResetPasswordScreen')}>
          <Text style={styles.forgot}>Forgot your password?</Text>
        </TouchableOpacity>
      </View> */}
      <Button title="Signup" onPress={onSignupPressed} color="" />
    </View>
  );
}

const styles = StyleSheet.create({
  loginWrapper: {
    backgroundColor: '#2196F3',
    flex: 1,
    padding: 20,
    justifyContent: 'center',
  },
  forgotPassword: {
    width: '100%',
    alignItems: 'flex-end',
    marginBottom: 24,
  },
  row: {
    flexDirection: 'row',
    marginTop: 4,
  },
  forgot: {
    fontSize: 13,
    color: 'white',
  },
  link: {
    fontWeight: 'bold',
    color: 'pink',
  },
});
