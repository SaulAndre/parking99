import React, {useContext, useState, useEffect} from 'react';
import {db, timestamp} from './config';
import auth from '@react-native-firebase/auth';
import {setDoc, doc} from 'firebase/firestore/lite';

const AuthContext = React.createContext();

export function useAuth() {
  return useContext(AuthContext);
}

// export function useUserToe

export function AuthProvider({children}) {
  const [currentUser, setCurrentUser] = useState();
  const [loading, setLoading] = useState(true);

  const signup = async (email, password, name, plateNumber) => {
    return auth()
      .createUserWithEmailAndPassword(email, password)
      .then(async res => {
        console.log('create-user-----------------\n', res);
        await setDoc(doc(db, 'users', res.user.uid), {
          id: res.user.uid,
          email: res.user.email,
          name: name,
          plateNumber: plateNumber.toUpperCase(),
          createdAt: timestamp.now(),
        });
      });
  };

  const login = (email, password) => {
    return auth().signInWithEmailAndPassword(email, password);
  };

  const logout = () => {
    return auth().signOut();
  };

  // const resetPassword = email => {
  //   return auth().sendPasswordResetEmail(email);
  // };

  // const updateEmail = email => {
  //   return currentUser.updateEmail(email);
  // };

  // const updatePassword = password => {
  //   return currentUser.updatePassword(password);
  // };

  useEffect(() => {
    const unsubscribe = auth().onAuthStateChanged(user => {
      setCurrentUser(user);
      setLoading(false);
    });

    return unsubscribe;
  }, []);

  const value = {
    currentUser,
    login,
    signup,
    logout,
    // resetPassword,
    // updateEmail,
    // updatePassword,
    // signInWithGoogle,
  };

  return (
    <AuthContext.Provider value={value}>
      {!loading && children}
    </AuthContext.Provider>
  );
}
