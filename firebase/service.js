import {db, timestamp} from './config';
import {
  collection,
  query,
  where,
  getDocs,
  setDoc,
  doc,
} from 'firebase/firestore/lite';
import {generateId} from '../utils';

const getAllParkiranName = async () => {
  var data = [];
  const q = query(collection(db, 'parkiran'));
  const querySnapshot = await getDocs(q);
  querySnapshot.forEach(doc => {
    data.push({
      id: doc.data().id,
      name: doc.data().name,
    });
  });
  return data;
};

const getAllParkiran = async () => {
  const q = query(collection(db, 'parkiran'));
  const querySnapshot = await getDocs(q);
  const listparkiran = querySnapshot.docs.map(doc => doc.data());
  return listparkiran;
};

const getParkiran = async parkiranId => {
  var data;
  const q = query(collection(db, 'parkiran'), where('id', '==', parkiranId));
  const querySnapshot = await getDocs(q);
  querySnapshot.docs.map(doc => {
    data = doc.data();
    console.log('service', doc.data());
  });
  console.log('parkiran-------\n', parkiranId);
  return data;
};

const getTicket = async ticketId => {
  var data;
  const q = query(collection(db, 'tiket'), where('id', '==', ticketId));
  const querySnapshot = await getDocs(q);
  querySnapshot.docs.map(doc => (data = doc.data()));
  return data;
};

const getActiveTicket = async userId => {
  var data;
  const q = query(
    collection(db, 'activeTicket'),
    where('userId', '==', userId),
  );
  const querySnapshot = await getDocs(q);
  querySnapshot.docs.map(doc => (data = doc.data()));
  return data;
};

const getBookingTicket = async userId => {
  var data;
  const q = query(
    collection(db, 'bookingTicket'),
    where('userId', '==', userId),
  );
  const querySnapshot = await getDocs(q);
  querySnapshot.docs.map(doc => (data = doc.data()));
  return data;
};

const getUser = async userId => {
  console.log(userId);
  var data;
  var q = query(collection(db, 'users'), where('id', '==', userId));
  var querySnapshot = await getDocs(q);
  querySnapshot.docs.map(doc => {
    data = doc.data();
  });
  return data;
};

const createTicket = async (parkiranId, userId, plateNumber) => {
  var activeTicketId = parkiranId.substring(0, 5) + userId.substring(0, 5);
  var uniqueId = generateId(20);
  await setDoc(doc(db, 'activeTicket', activeTicketId), {
    id: activeTicketId,
    ticketId: uniqueId,
    parkiranId: parkiranId,
    userId: userId,
    checkIn: timestamp.now(),
    plateNumber: plateNumber,
  }).catch(err => {
    console.log(err.code);
  });
  await setDoc(doc(db, 'tiket', uniqueId), {
    id: uniqueId,
    status: 'unpaid',
    parkiranId: parkiranId,
    checkIn: timestamp.now(),
    createdAt: timestamp.now(),
    userId: userId,
    plateNumber: plateNumber,
  }).catch(err => {
    console.log(err);
    return err;
  });
  return uniqueId;
};

const setBooking = async (
  parkiranId,
  userId,
  checkIn,
  checkOut,
  downPayment,
  plateNumber,
) => {
  var bookingTicketId = parkiranId.substring(0, 5) + userId.substring(0, 5);
  var uniqueId = generateId(20);
  await setDoc(doc(db, 'bookingTicket', bookingTicketId), {
    id: bookingTicketId,
    parkiranId: parkiranId,
    userId: userId,
    ticketId: uniqueId,
    createAt: timestamp.now(),
    estCheckIn: timestamp.fromDate(checkIn),
    estCheckOut: timestamp.fromDate(checkOut),
    downPayment: downPayment,
    plateNumber: plateNumber,
  });
  await setDoc(doc(db, 'tiket', uniqueId), {
    id: uniqueId,
    status: 'booking',
    parkiranId: parkiranId,
    userId: userId,
    plateNumber: plateNumber,
    downPayment: downPayment,
    createAt: timestamp.now(),
  }).catch(err => {
    console.log(err);
    return err;
  });
  return uniqueId;
};

export {
  getAllParkiran,
  getAllParkiranName,
  getParkiran,
  getTicket,
  getActiveTicket,
  createTicket,
  getUser,
  setBooking,
  getBookingTicket,
};
